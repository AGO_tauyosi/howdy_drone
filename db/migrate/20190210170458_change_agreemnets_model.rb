class ChangeAgreemnetsModel < ActiveRecord::Migration[5.2]
  def change
    remove_column :agreements, :place
    add_column :agreements, :place, :string
    add_column :agreements, :contact, :string
  end
end
