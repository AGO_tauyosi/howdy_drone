class CreatePilots < ActiveRecord::Migration[5.2]
  def change
    create_table :pilots do |t|
      t.integer :user_id
      t.string :rank
      t.text :adress
      t.text :skill
      t.text :tools
      t.string :price
      t.string :integer
      t.text :insuarance
      t.text :message

      t.timestamps
    end
  end
end
