class AddPilotFlgToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :pilot_flg, :boolean
  end
end
