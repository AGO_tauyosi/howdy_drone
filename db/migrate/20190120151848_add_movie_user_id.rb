class AddMovieUserId < ActiveRecord::Migration[5.2]
  def change
    add_column :movies, :user_id, :integer
    add_column :movies, :pilot_id, :integer
    remove_column :movies, :pilot
    remove_column :pilots, :integer
  end
end
