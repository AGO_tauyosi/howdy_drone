class AddTagToMovies < ActiveRecord::Migration[5.2]
  def change
    add_column :movies, :tag, :string
  end
end
