class AddNameToPilot < ActiveRecord::Migration[5.2]
  def change
    add_column :pilots, :name, :string
  end
end
