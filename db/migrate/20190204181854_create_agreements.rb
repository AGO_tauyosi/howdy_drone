class CreateAgreements < ActiveRecord::Migration[5.2]
  def change
    create_table :agreements do |t|
      t.integer :user_id
      t.integer :pilot_id
      t.integer :price
      t.datetime :date
      t.datetime :due_date
      t.text :other
      t.boolean :agreement_flag

      t.timestamps
    end
  end
end
