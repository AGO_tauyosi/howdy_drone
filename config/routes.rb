Rails.application.routes.draw do


  get 'users/:id/show' => "users#show"
  post 'comments/:id/create' => "comments#create"
  post 'comments/:id/destroy' => "comments#destroy"
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users
  resources :users, :only => [:index, :show]
  resources :charges
  resources :agreements, :only => [:create]
  resources :messages, :only => [:create, :destroy]
  resources :rooms, :only => [:create, :destroy, :show]
  
  get '/' => "home#top"
  get '/about' => "home#about"
  get '/terms' => "home#terms"
  get '/privacy' => "home#privacy"
  get '/flow' => "home#flow"
  get '/ctl' => "home#ctl"
  get '/contact' => "home#contact"
  get '/signup' => "home#signup"
  get '/mypage' => "home#mypage"


  get 'movies/index' => "movies#index"
  get 'movies/:id/show' => "movies#show"
  get 'movies/wedding_more' => "movies#wedding_more"
  get 'movies/travel_more' => "movies#travel_more"
  get 'movies/anniversary_more' => "movies#anniversary_more"
  get 'movies/other_more' => "movies#other_more"
  get 'movies/new' => "movies#new"
  get 'movies/:id/new' => "movies#new2"
  post 'movies/create' => "movies#create"

  get 'pilots/index' => "pilots#index"
  get 'pilots/:id/show' => "pilots#show"
  get 'pilots/offer' => "pilots#offer"
  get 'pilots/reservation' => "pilots#reservation"
  get 'pilots/ace_more' => "pilots#ace_more"
  get 'pilots/pro_more' => "pilots#pro_more"
  get 'pilots/amature_more' => "pilots#amature_more"
  get 'pilots/new' => "pilots#new"
  post 'pilots/create' => "pilots#create"
  
  get 'agreements/:id/new' => "agreements#new"  
  get 'agreements/:id/show' => "agreements#show"
  post 'agreements/:id/agree' => "agreements#agree"
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
