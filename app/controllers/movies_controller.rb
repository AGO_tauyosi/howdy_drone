class MoviesController < ApplicationController
  
  before_action :authenticate_user!, only: [:create, :show, :new, :new2]
  
  def index
    @movies = Movie.all
    @pilots = Pilot.all
    @weddingMovies = Movie.where(tag: "wedding")
    @travelMovies = Movie.where(tag: "travel")
    @anniversaryMovies = Movie.where(tag: "anniversary")
    @otherMovies = Movie.where(tag: "other")
  end
  
  def show
    @comments = Comment.where(movie_id: params[:id])
    @movie = Movie.find_by(id: params[:id])
    @relevantMovies = Movie.where(tag: @movie.tag).where.not(id: @movie.id)
  end
  
  def wedding_more
    @movies = Movie.where(tag: "wedding")
  end
  
  def travel_more
    @movies = Movie.where(tag: "travel")
  end
  
  def anniversary_more
    @movies = Movie.where(tag: "anniversary")
  end
  
  def other_more
    @movies = Movie.where(tag: "other")
  end
  
  def new
    @movie = Movie.new
  end
  
  def new2
    @movie = Movie.new
    @pilot = Pilot.find_by(id: params[:id])
  end
  
  def create
    @pilot = Pilot.find_by(id: params[:pilot_id])
    if @pilot.name == params[:pilot_name]
      @movie = Movie.new(title: params[:title], url: params[:url], tag: params[:tag], user_id: @pilot.user_id, pilot_id: @pilot.id)
      if @movie.save
        flash[:notice] = "動画を投稿しました"
        redirect_to("/movies/new")
      else
        render("movies/new")
      end
    else
      @movie = Movie.new
      redirect_to("/movies/new")
      flash[:notice] = "パイロットIDとパイロット名が一致しません"
    end
  end
  
end
