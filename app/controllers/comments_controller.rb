class CommentsController < ApplicationController
  
  def create
    @movie = Movie.find_by(id: params[:id])
    @receiver = User.find_by(id: @movie.user_id)
    @comment = Comment.new(content: params[:content], user_id: current_user.id, movie_id: @movie.id)
    if @comment.save
      UserMailer.comment_mail(@receiver,@comment,@movie).deliver_now
      redirect_to("/movies/#{@movie.id}/show")
    else
      render("movies/index")
    end
  end
  
  def destroy
    @comment = Comment.find_by(id: params[:id])
    if @comment.destroy
      redirect_to("/movies/#{@comment.movie_id}/show")
    else
      render("movies/index")
    end
  end
end