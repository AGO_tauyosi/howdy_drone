class HomeController < ApplicationController
  def top
  end
  def about
  end
  def terms
  end
  def privacy
  end
  def ctl
  end
  def flow
  end
  def contact
  end
  def mypage
    @entries = Entry.where(user: current_user.id)
    @agreements_u = Agreement.where(user_id: current_user.id)
    @agreements_p = Agreement.where(pilot_id: current_user.id)
  end
end
