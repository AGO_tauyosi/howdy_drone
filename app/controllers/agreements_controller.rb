class AgreementsController < ApplicationController
  
  def new
    @user = User.find_by(id: params[:id])
  end
  
  def create
    @receiver = User.find_by(id: params[:user_id])
    @agreement = Agreement.new(user_id: params[:user_id], pilot_id: current_user.id, price: params[:price], date: params[:date], due_date: params[:due_date], place: params[:place], contact: params[:contact], other: params[:other])
    if @agreement.save
      UserMailer.agreement_mail(@receiver,@agreement).deliver_now
      flash[:notice] = "契約を確定しました"
      redirect_to("/mypage")
    else
    end
  end
  
  def show
    @agreement = Agreement.find_by(id: params[:id])
    @user = User.find_by(id: @agreement.user_id)
    @pilot = Pilot.find_by(user_id: @agreement.pilot_id)
  end
  
  def agree
    @agreement = Agreement.find_by(id: params[:id])
    @receiver = User.find_by(id: @agreement.pilot_id)
    if @agreement.update_attribute(:agreement_flag, true)
      UserMailer.agree_mail(@receiver,@agreement).deliver_now
      flash[:notice] = "契約を確定しました"
      redirect_to("/mypage")
    else
    end
  end
  
end
