class MessagesController < ApplicationController
  before_action :authenticate_user!, :only => [:create]

  def create
    if Entry.where(:user_id => current_user.id, :room_id => params[:message][:room_id]).present?
      @message = Message.create(params.require(:message).permit(:user_id, :content, :room_id).merge(:user_id => current_user.id))
      @room = Room.find_by(id: @message.room_id)
      @entries = @room.entries
      @entries.each do |e|
        @receiver = User.find_by(id: e.user_id)
        if @receiver.id != current_user.id
          UserMailer.message_mail(@receiver,@message).deliver_now
        end
      end
      redirect_to "/rooms/#{@message.room_id}"
    else
      redirect_back(fallback_location: root_path)
    end
  end
  
end
