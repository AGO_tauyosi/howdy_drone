class ChargesController < ApplicationController
  
  def new
    @amount = 10000
    @pilot = "パイロット"
  end

  def create
    
    @amount = params[:amount]
    @pilot = params[:pilot]
  
    customer = Stripe::Customer.create(
      :email => params[:stripeEmail],
      :source  => params[:stripeToken]
    )

    charge = Stripe::Charge.create(
      :customer    => customer.id,
      :amount      => @amount,
      :description => @pilot,
      :currency    => 'jpy'
    )
  

  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to new_charge_path
  end
end

