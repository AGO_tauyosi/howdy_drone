class PilotsController < ApplicationController

before_action :authenticate_user!, only: [:create, :show, :new]


  def index
    @pilots = Pilot.all
    @acePilots = Pilot.where(rank: "ace")
    @proPilots = Pilot.where(rank: "pro")
    @amaturePilots = Pilot.where(rank: "amature")
  end

  def show
    @pilot = Pilot.find_by(id: params[:id])
    @user = User.find_by(id: @pilot.user_id)
    @relevantMovies = Movie.where(pilot_id: @pilot.id)
  end

  def ace_more
  end

  def pro_more
  end

  def amature_more
  end

  def new
    @pilot = Pilot.new
  end

  def create
    @user = User.find_by(id: params[:user_id])
    if @user.name == params[:name]
      @pilot = Pilot.new(user_id: params[:user_id], name: params[:name], rank: params[:rank], adress: params[:adress], skill: params[:skill], tools: params[:tools], price: params[:price], insuarance: params[:insuarance], message: params[:message])
      @user.pilot_flg = true
      if @pilot.save && @user.save
        flash[:notice] = "パイロットを登録しました"
        redirect_to("/pilots/new")
      else
        render("pilots/new")
      end
    else
      @pilot = Pilot.new
      redirect_to("/pilots/new")
      flash[:notice] = "ユーザーIDとユーザー名が一致しません"
    end
  end

end

