class UserMailer < ApplicationMailer
  default from: "ago.web.consultant@gmail.com"
  
  def comment_mail(receiver,comment,movie)
    @receiver = receiver
    @comment = comment
    @movie = movie
    mail(
      to:   @receiver.email,
      subject: 'コメント通知'
    )
  end
  
  def message_mail(receiver,message)
    @receiver = receiver
    @message = message
    mail(
      to:   @receiver.email,
      subject: 'メッセージ通知'
    )    
  end
  
  def agreement_mail(receiver,agreement)
    @receiver = receiver
    @agreement = agreement
    mail(
      to:   @receiver.email,
      subject: '見積もり通知'
    )
  end  
  
  def agree_mail(receiver,agreement)
    @receiver = receiver
    @agreement = agreement
    mail(
      to:   @receiver.email,
      subject: '契約通知'
    )
  end
  
end
