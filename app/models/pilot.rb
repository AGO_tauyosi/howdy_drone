class Pilot < ApplicationRecord
  validates :user_id,{presence: true, uniqueness: true}
  validates :name,{presence: true}
  validates :rank,{presence: true}
  validates :adress,{presence: true}
  validates :skill,{presence: true}
  validates :tools,{presence: true}
  validates :price,{presence: true}
  validates :insuarance,{presence: true}
  validates :message,{presence: true}
  belongs_to :user
  has_many :movies
end
