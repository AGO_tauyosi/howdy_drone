class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  validates :name, presence: true
  has_many :messages, dependent: :destroy
  has_many :entries, dependent: :destroy
  has_many :movies, dependent: :destroy
  has_many :pilots, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :agreements, dependent: :destroy
  include Gravtastic
    gravtastic
end
