class Movie < ApplicationRecord
  validates :title,{presence: true}
  validates :user_id,{presence: true}
  validates :pilot_id,{presence: true}
  validates :url,{presence: true}
  validates :tag,{presence: true}
  belongs_to :user
  belongs_to :pilot
  has_many :comments
end
