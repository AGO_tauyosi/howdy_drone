// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require rails-ujs
//= require bxslider
//= require activestorage
//= require bootstrap-sprockets
//= require_tree

jQuery(function($){
  
    $('.bg-slider').bgSwitcher({
        images: ['/slide1.jpg','/slide2.jpg','/slide3.jpg','/slide4.jpg'], // 切替背景画像を指定
	      interval: 10000, // 背景画像を切り替える間隔を指定 3000=3秒
        loop: true, // 切り替えを繰り返すか指定 true=繰り返す false=繰り返さない
        shuffle: false, // 背景画像の順番をシャッフルするか指定 true=する false=しない
        effect: "fade", // エフェクトの種類をfade,blind,clip,slide,drop,hideから指定
        duration: 500, // エフェクトの時間を指定します
        easing: "swing" // エフェクトのイージングをlinear,swingから指定
    });
  
    var startPos = 0,winScrollTop = 0;
    $(window).on('scroll',function(){
    winScrollTop = $(this).scrollTop();
      if (winScrollTop >= startPos) {
        if(winScrollTop >= 300){
        $('header').addClass('hide');
        }
      } else {
        $('header').removeClass('hide');
      }
      startPos = winScrollTop;
    });
  
    function func_switch_jq_ani(id){
      var $ele = $(id);
      if($ele.length == 0)return;
      $ele.animate({opacity:'toggle',},{duration:1000,easing:'linear',})
      setTimeout(function(elemld){
        func_switch_jq_ani(elemld);
      },500,id);
    }
    $(function(){
      func_switch_jq_ani("#switch_jq_ani");
    });


    $('#menu-modal-open').click(function(){
        $('#menu-modal-main').show();
    });
    $('#menu-modal-close').click(function(){
        $('#menu-modal-main').hide();
    });

    var $win = $(window);
    $win.on('load resize', function(){
      var windowWidth = window.innerWidth;

      if(windowWidth > 992){
        $(window).fadeThis();
      }
    });

    
    $('#login-push').click(function(){
        $('#login-modal').fadeIn();
    });
    
   $(window).on('load', function() {
        $('#loader-bg').hide();
    });
    
    
    
    $(document).ready(function(){
      var $win = $(window);
      $win.on('load resize', function(){
        var windowWidth = window.innerWidth;

        if(windowWidth > 1200){//PC
         $('.bxslider').bxSlider({
          auto: true,           // 自動スライド
          speed: 1000,          // スライドするスピード
          moveSlides: 1,        // 移動するスライド数
          pause: 5000,          // 自動スライドの待ち時間
          maxSlides: 4,         // 一度に表示させる最大数
          slideWidth: 250,      // 各スライドの幅
      	  randomStart: true,    // 最初に表示するスライドをランダムに設定
          adaptiveHeight: false,
          autoHover: true,       // ホバー時に自動スライドを停止
          slideMargin: 30
          });
          
        }else if(windowWidth > 991){//PC(小)
          $('.bxslider').bxSlider({
          auto: true,           // 自動スライド
          speed: 1000,          // スライドするスピード
          moveSlides: 1,        // 移動するスライド数
          pause: 5000,          // 自動スライドの待ち時間
          maxSlides: 3,         // 一度に表示させる最大数
          slideWidth: 250,      // 各スライドの幅
      	  randomStart: true,    // 最初に表示するスライドをランダムに設定
          adaptiveHeight: false,
          autoHover: true,       // ホバー時に自動スライドを停止
          slideMargin: 30
          });
          
        }else if(windowWidth > 767){//タブレット
          $('.bxslider').bxSlider({
          auto: true,           // 自動スライド
          speed: 1000,          // スライドするスピード
          moveSlides: 1,        // 移動するスライド数
          pause: 5000,          // 自動スライドの待ち時間
          maxSlides: 2,         // 一度に表示させる最大数
          slideWidth: 250,      // 各スライドの幅
      	  randomStart: true,    // 最初に表示するスライドをランダムに設定
          adaptiveHeight: false,
          autoHover: true,       // ホバー時に自動スライドを停止 
          slideMargin: 30
          });
        }else{   
       $('.bxslider').bxSlider({//スマホ コンテンツ幅560
          auto: true,           // 自動スライド
          speed: 1000,          // スライドするスピード
          moveSlides: 1,        // 移動するスライド数
          pause: 5000,          // 自動スライドの待ち時間
          maxSlides: 1,         // 一度に表示させる最大数
          slideWidth: 300,      // 各スライドの幅
      	  randomStart: true,    // 最初に表示するスライドをランダムに設定
          adaptiveHeight: false,
          autoHover: true,  // ホバー時に自動スライドを停止 
          slideMargin: 10
          });
        }
      });
    });
});
